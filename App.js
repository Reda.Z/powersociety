import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, View, StatusBar, Dimensions,Image } from 'react-native';
import * as ScreenOrientation from 'expo-screen-orientation';
import AsyncStorage from "@react-native-async-storage/async-storage";
import { ActivityIndicator, Divider, HelperText, Title } from 'react-native-paper';
import * as Device from 'expo-device';
import * as Brightness from 'expo-brightness';
import * as Location from 'expo-location';
import Content from './Content';
import { WebView } from 'react-native-webview';

export default function App() {

  const [Code, setCode] = useState("");
  const [Playlist, setPlayList] = useState();
  const [Status, setStatus] = useState(true);

  useEffect(() => {

    StatusBar.setHidden(true);
    ScreenOrientation.lockAsync(ScreenOrientation.OrientationLock.LANDSCAPE_LEFT);
    try {
      AsyncStorage.getItem("Code")
      .then((response) => {
        if(response == null){
          (async() => {  
            let { status } = await Location.requestForegroundPermissionsAsync();
            if (status !== 'granted') {
              setErrorMsg('Permission to access location was denied');
              return;
            }
      
            let location = await Location.getCurrentPositionAsync({});
            const height = Math.round(Dimensions.get("screen").height);
            const width = Math.round(Dimensions.get("screen").width);
            const brightness = await Brightness.getBrightnessAsync();
            const Mode = await Brightness.getSystemBrightnessModeAsync();
            generateCode(Mode,brightness,width,height,location);
      
          })();
        } else {
          setCode(response)
          getStatus(response)
        }
      }) 
    } catch (e) {
      console.log(e)
    }
    
  },[])

  const generateCode = (Mode,brightness,width,height,location) => {

    const Data =new FormData();
    Data.append("operating_system","Android");
    Data.append("player_version",Device.osVersion);
    Data.append("screen_res",width.toString()+"x"+height.toString());
    Data.append("volume","50");
    Data.append("brightness",(Mode==1? "auto" : Math.round(brightness*100)));
    Data.append("location",location.coords.latitude.toString()+","+location.coords.longitude.toString());
    fetch(
      `https://powerscreentouch.com/api/generate_code.php`,{
        method: "post",
        body: Data 
      } 
    )
    .then(res => res.json())
    .then((resJson) =>  {
      console.log(resJson)
      if(resJson.success){
        setCode(resJson.results.code);
        getStatus(resJson.results.code);
        AsyncStorage.setItem("Code",resJson.results.code);
      }
    })
    .catch((e) => {console.log(e+Data)})
  };

  const getStatus = (Code) =>  {
    
    fetch(
      `https://powerscreentouch.com/api/is_active_Mobile.php?code=${Code}`
    )
    .then(res => res.json())
    .then((resJson) => {
      console.log(resJson)
      if(resJson.success){
        setStatus(resJson.active);
      }
    })
    .catch((e) => {})
    const intervalId = setInterval(() => {
      fetch(
        `https://powerscreentouch.com/api/is_active_Mobile.php?code=${Code}`
      )
      .then(res => res.json())
      .then((resJson) => {
        console.log(resJson)
        if(resJson.success){
          if(resJson.active==true){
            clearInterval(intervalId)
          }
          setStatus(resJson.active);
        }
      })
      .catch((e) => {})
    }, 10000)
  };
 
  return (
    <View style={{flex:1}}>
        {Code? (
          <>
            {Status? (
              <WebView
                source={{uri: "https://youtu.be/sPgqEHsONK8"}}
                scrollEnabled={false}
                allowsFullscreenVideo
                style={{
                  height:"100%",
                  width:"100%"
                }}
              />
            ) : (
              <>
                <View style={styles.upper}>
                  <Divider style={styles.divider}/>
                  <Title style={styles.title}>Initialization</Title>
                  <HelperText style={styles.helper}>Activez l'écran sur le site web https://powerscreentouch.com en utilisant le code </HelperText>
                </View>
                <View style={styles.lower}>
                  <Title style={styles.codeText}>{Code}</Title>
                </View>
              </>
            )}
          </>
        ) : (
         <>
          <Image
            source={require("./assets/pocIcon.png")}
            resizeMode="contain"
            style={{
              height:"100%",
              width:"100%",
              alignSelf:"center"
            }}
          />
         </>
        )} 
    </View>
  );
}

const styles = StyleSheet.create({
  upper:{
    flex:1,
    backgroundColor: "gray",
  },
  lower:{
    flex:2,
    backgroundColor:"black",
    justifyContent:"center"
  },
  divider:{
    backgroundColor:"red",
    height:2,
    width:"25%",
    marginLeft:30,
    marginTop:30
  },
  title:{
    marginLeft:30,
    color:"white",
    fontWeight:"bold",
    fontSize:25,
    marginVertical:5
  },
  helper:{
    marginLeft:30,
    color:"white",
    fontSize:13
  },
  codeText:{
    alignSelf:"center", 
    color:"white", 
    fontSize:100, 
    paddingTop:100
  }
});
